<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('/hello/world', function () use ($app) {
    return "Hello World!";
});

$app->get('/hello/{name}', ['middleware' => 'hello', function ($name) use ($app) {
    // Log::info("test");
    return "Hello {$name}";
}]);

$app->get('/request', function (Illuminate\Http\Request $request) {
    return "Hello " . $request->get('name', 'Animal');
});

// $app->get('/response', function (Illuminate\Http\Request $request) {
//     return (new Illuminate\Http\Response('Hello Animal', 200))
//         ->header('Content-Type', 'text/plain');
// });

$app->get('/response', function (Illuminate\Http\Request $request) {
    if ($request->wantsJson()) {
        return response()->json(['greeting' => 'Hello Hello']);
    }
    // return (new Illuminate\Http\Response('Hello Hello 200', 200))
    //     ->header('Content-Type', 'text/plain');
    // });
    return response()
         ->make('Hello stranger 200', 200, ['Content-Type' => 'text/plain']);
    });
