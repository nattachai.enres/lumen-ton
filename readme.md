# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/albofish/footstep/v)](https://packagist.org/packages/laravel/lumen-framework#v5.2.0)
[![Latest Unstable Version](https://poser.pugx.org/albofish/footstep/v/unstable)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

[![82ca11e645ad5726e498525c68cece94.jpg](https://i.postimg.cc/FF0F60p5/82ca11e645ad5726e498525c68cece94.jpg)](https://postimg.cc/ThYXLLBC)

The Reference [Lumen Programming Guide](https://www.amazon.com/Lumen-Programming-Guide-Writing-Microservices/dp/1484221869).

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
